﻿using StateMachine;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAction : MonoBehaviour
{
    [SerializeField] private ActionIndex _actionIndex;

    private void Start()
    {
        Button thisButton = GetComponent<Button>();

        if (thisButton != null)
        {
            thisButton.onClick.AddListener(() =>
            {
                ChangeAction(_actionIndex);
            });
        }
        else
        {
            Debug.LogError("Компонент ButtonAction необходмо установить на элемент с кнопкой");
        }
    }

    public void ChangeAction(ActionIndex index)
    {
        Translator.SendEvent(index);
    }
}
