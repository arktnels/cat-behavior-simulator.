﻿using StateMachine;
using UnityEngine;
using UnityEngine.UI;

public class Cat : MonoBehaviour
{
    [SerializeField] private Text _description;
    [SerializeField] private Text _mood;
    public Animator animator;

    private MoodStateMachine _stateMachine;
    [SerializeField] private StatesList states;

    private void OnValidate()
    {
        if (animator == null)
            animator = GetComponent<Animator>();
    }

    void Start()
    {
        Translator.AddListener(ChangeAction);

        _stateMachine = new MoodStateMachine();

        states.StateInit(_stateMachine, this);

        _stateMachine.Init(states.GetState(StateIndex.Good));

        _description.text = "";
        _mood.text = "";
    }

    void Update()
    {
        
    }

    private void OnDestroy()
    {
        Translator.RemoveListener(ChangeAction);
    }

    public void ChangeAction(ActionIndex action)
    {
        _stateMachine.StateSetAction(action);
    }

    public void SetDescription(string description)
    {
        _description.text = description;
    }

    public void SetViewState(string nameState)
    {
        _mood.text = nameState;
    }

    public void SetState(StateIndex state)
    {
        _stateMachine.ChangeState(states.GetState(state));
    }

    public void SetAnimation(AnimType animType)
    {
        CatAnimator.SetAnimation(animator, animType);
    }
}
