﻿using UnityEngine;

public enum AnimType
{
    Idle,
    Walk,
    Sound,
    Eat,
    Jump
}
