﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuLobby : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void BackLobby()
    {
        SceneManager.LoadScene("LoadingLobby");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
