﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/ParticleData", fileName = "Particle Data", order = 51)]
public class ParticleData : ScriptableObject
{
    public int countParticles;
    public float gravity;
    public Color startColor;
    public Color endColor;
    public Material material;
}
