﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    [SerializeField] private ParticleData _particleDataUp;
    [SerializeField] private ParticleData _particleDataDown;
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private ParticleSystemRenderer particleRender;

    private void OnValidate()
    {
        if (particle == null)
            particle = GetComponent<ParticleSystem>();

        if (particleRender == null)
            particleRender = GetComponent<ParticleSystemRenderer>();
    }

    private void Start()
    {
        Translator.AddListener(Checkparticle);
    }

    private void OnDestroy()
    {
        Translator.RemoveListener(Checkparticle);
    }

    public void Checkparticle(bool isUp)
    {
        if (isUp)
            SetParticle(_particleDataUp);
        else
            SetParticle(_particleDataDown);

        particle.Play();
    }

    private void SetParticle(ParticleData data)
    {
        var main = particle.main;
        main.gravityModifier = data.gravity;

        var partColor = particle.colorOverLifetime.color;
        partColor.colorMin = data.startColor;
        partColor.colorMax = data.endColor;

        var emission = particle.emission;
        emission.rate = data.countParticles;

        particleRender.material = data.material;
    }
}
