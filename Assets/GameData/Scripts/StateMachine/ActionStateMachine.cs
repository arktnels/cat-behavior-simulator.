﻿using UnityEngine;

namespace StateMachine
{
    public class ActionStateMachine
    {
        public Action CurrentAction { get; private set; }

        public void Init()
        {
        }

        public void ChangeAction(Action newAction)
        {
            if (newAction == null)
                return;

            CurrentAction?.ExitAction();

            CurrentAction = newAction;
            CurrentAction.EnterAction();
        }

        public void UpdateAction()
        {
            CurrentAction.UpdateAction();
        }

        public void PhysicsUpdateAction()
        {
            
        }
    }
}