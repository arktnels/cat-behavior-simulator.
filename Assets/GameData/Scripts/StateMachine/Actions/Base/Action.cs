﻿using UnityEngine;

namespace StateMachine
{
    public class Action : ScriptableObject
    {
        [SerializeField] protected string description = "_";

        [SerializeField] protected ActionIndex _indexAction;
        [SerializeField] protected State _nextState;
        protected Cat _character;

        public ActionIndex ActionIndex => _indexAction;

        public void Init(Cat character)
        {
            _character = character;
        }

        public virtual void EnterAction()
        {
            _character.SetDescription(description);
            _character.SetState(_nextState.NameState);
        }

        public virtual void UpdateAction()
        {

        }

        public virtual void ExitAction()
        {

        }
    }
}