﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/Action/Purrs And Wags", fileName = "Purrs And Wags", order = 51)]
    public class PurrsAndWags : Action
    {
        public override void EnterAction()
        {
            base.EnterAction();
            _character.SetAnimation(AnimType.Sound);
        }
    }
}