﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/Action/Eats Everything", fileName = "Eats Everything", order = 51)]
    public class EatsEverything : Action
    {
        public override void EnterAction()
        {
            base.EnterAction();
            _character.SetAnimation(AnimType.Eat);
        }
    }
}