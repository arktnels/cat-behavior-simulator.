﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/Action/Eats Quickly", fileName = "Eats Quickly", order = 51)]
    public class EatsQuickly : Action
    {
        public override void EnterAction()
        {
            base.EnterAction();
            _character.SetAnimation(AnimType.Eat);
        }
    }
}