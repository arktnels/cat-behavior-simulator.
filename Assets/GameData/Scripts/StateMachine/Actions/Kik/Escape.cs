﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/Action/Escape", fileName = "Escape", order = 51)]
    public class Escape : Action
    {
        public override void EnterAction()
        {
            base.EnterAction();
            _character.SetAnimation(AnimType.Walk);
        }
    }
}