﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/Action/Escape And Pisses", fileName = "Escape And Pisses", order = 51)]
    public class EscapeAndPisses : Action
    {
        public override void EnterAction()
        {
            base.EnterAction();
            _character.SetAnimation(AnimType.Walk);
        }
    }
}