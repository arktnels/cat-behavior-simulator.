﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/Action/Jumps And Bites", fileName = "Jumps And Bites", order = 51)]
    public class JumpsAndBites : Action
    {
        public override void EnterAction()
        {
            base.EnterAction();
            _character.SetAnimation(AnimType.Jump);
        }
    }
}