﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/Action/Runs Slowly", fileName = "Runs Slowly", order = 51)]
    public class RunsSlowly : Action
    {
        public override void EnterAction()
        {
            base.EnterAction();
            _character.SetAnimation(AnimType.Walk);
        }
    }
}