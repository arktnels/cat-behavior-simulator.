﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/Action/Sitting Still", fileName = "Sitting Still", order = 51)]
    public class SittingStill : Action
    {
        public override void EnterAction()
        {
            base.EnterAction();
            _character.SetAnimation(AnimType.Idle);
        }
    }
}