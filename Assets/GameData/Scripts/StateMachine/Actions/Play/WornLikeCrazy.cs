﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/Action/Worn Like Crazy", fileName = "Worn Like Crazy", order = 51)]
    public class WornLikeCrazy : Action
    {
        public override void EnterAction()
        {
            base.EnterAction();
            _character.SetAnimation(AnimType.Walk);
        }
    }
}