﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "/State Machine/Action/Play Action", fileName = "Play Action", order = 51)]
    public class PlayAction : Action
    {
        public override void EnterAction()
        {
            base.EnterAction();
        }

        public override void UpdateAction()
        {
            base.UpdateAction();
        }

        public override void ExitAction()
        {
            base.ExitAction();
        }
    }
}