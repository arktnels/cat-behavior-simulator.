﻿using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/List Actions", fileName = "List Actions", order = 51)]
    public class ActionsList : ScriptableObject
    {
        public Action[] actions;
        private Dictionary<ActionIndex, Action> _tableActions;

        public void ActionInit(Cat character)
        {
            _tableActions = new Dictionary<ActionIndex, Action>();
            foreach (Action action in actions)
            {
                if (action != null && !_tableActions.ContainsKey(action.ActionIndex))
                {
                    _tableActions.Add(action.ActionIndex, action);
                    _tableActions[action.ActionIndex].Init(character);
                }
            }
        }

        public Action GetAction(ActionIndex index)
        {
            return (_tableActions.ContainsKey(index)) ? _tableActions[index] : null;
        }
    }
}