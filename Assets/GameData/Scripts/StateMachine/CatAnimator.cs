﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatAnimator
{
    public static void SetAnimation(Animator anim, AnimType animation)
    {
        anim.Play(animation.ToString());
    }
}
