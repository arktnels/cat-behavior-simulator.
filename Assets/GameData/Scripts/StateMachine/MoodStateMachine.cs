﻿using UnityEngine;

namespace StateMachine
{
    public class MoodStateMachine
    {
        public State CurrentState { get; private set; }

        public void Init(State startingState)
        {
            CurrentState = startingState;
            CurrentState.Enter();
        }

        public void ChangeState(State newState)
        {
            if (newState == null || newState.Equals(CurrentState))
                return;

            if (newState.NameState > CurrentState.NameState)
                Translator.SendEvent(true);
            else if (newState.NameState < CurrentState.NameState)
                Translator.SendEvent(false);

            CurrentState.Exit();

            CurrentState = newState;
            CurrentState.Enter();
        }

        public void StateUpdate()
        {
            CurrentState.StateUpdate();
        }

        public void StatePhysicsUpdate()
        {
            CurrentState.StatePhysicUpdate();
        }

        public void StateSetAction(ActionIndex index)
        {
            CurrentState.SetAction(index);
        }
    }
}