﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/State/Bad Mood", fileName = "Bad Mood", order = 51)]
    public class BadMood : State
    {

    }
}