﻿using UnityEngine;

namespace StateMachine
{
    public abstract class State : ScriptableObject
    {
        protected Cat _character;
        [SerializeField] protected StateIndex _indexState;
        [SerializeField] protected ActionsList _actions;

        protected ActionStateMachine _stateMachine;

        public ActionsList ActionsList => _actions;
        public StateIndex NameState => _indexState;
        public void SetNameState(StateIndex value) => _indexState = value;

        public virtual void Enter()
        {
            _character.SetViewState(NameState.ToString());
        }

        public virtual void Exit()
        {
        }

        public virtual void StateInit(Cat character)
        {
            _character = character;
            _stateMachine = new ActionStateMachine();
            _actions.ActionInit(character);
        }

        public virtual void StateUpdate() { }

        public virtual void StatePhysicUpdate() { }

        public virtual void SetAction(ActionIndex index)
        {
            _stateMachine.ChangeAction(_actions.GetAction(index));
        }

        public void SetActionList(ActionsList actions)
        {
            _actions = actions;
        }
    }
}