﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/State/Excellent Mood", fileName = "Excellent Mood", order = 51)]
    public class ExcellentMood : State
    {

    }
}