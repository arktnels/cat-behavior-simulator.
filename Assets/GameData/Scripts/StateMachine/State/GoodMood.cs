﻿using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/State/Good Mood", fileName = "Good Mood", order = 51)]
    public class GoodMood : State
    {

    }
}