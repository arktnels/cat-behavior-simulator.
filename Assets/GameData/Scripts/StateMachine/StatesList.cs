﻿using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "State Machine/List Stats", fileName = "List Stats", order = 51)]
    public class StatesList : ScriptableObject
    {
        public State[] states;
        private Dictionary<StateIndex, State> _tableStates;

        public void StateInit(MoodStateMachine stateMachine, Cat cat)
        {
            _tableStates = new Dictionary<StateIndex, State>();
            foreach (State state in states)
            {
                State st = CreateInstance(state.GetType()) as State;
                st.SetNameState(state.NameState);
                _tableStates.Add(state.NameState, st);
                _tableStates[state.NameState].SetActionList(state.ActionsList);
                _tableStates[state.NameState].StateInit(cat);
            }
        }

        public State GetState(StateIndex index)
        {
            return (_tableStates.ContainsKey(index)) ? _tableStates[index] : null;
        }

        private void OnDestroy()
        {
            Queue<State> states = new Queue<State>(_tableStates.Values);

            while (states.Count > 0)
                DestroyImmediate(states.Dequeue());
        }
    }
}