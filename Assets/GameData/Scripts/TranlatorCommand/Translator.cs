﻿using StateMachine;

public delegate void ActionEvent(ActionIndex index);
public delegate void ParticleEvent(bool isUp);
public class Translator
{
    private static event ActionEvent onActionEvent;
    private static event ParticleEvent onParticleEvent;

    public static void AddListener(ActionEvent listener)
    {
        if (listener != null)
            onActionEvent += listener;
    }

    public static void RemoveListener(ActionEvent listener)
    {
        if (listener != null)
            onActionEvent -= listener;
    }

    public static void SendEvent(ActionIndex index)
    {
        onActionEvent?.Invoke(index);
    }

    public static void AddListener(ParticleEvent listener)
    {
        if (listener != null)
            onParticleEvent += listener;
    }

    public static void RemoveListener(ParticleEvent listener)
    {
        if (listener != null)
            onParticleEvent -= listener;
    }

    public static void SendEvent(bool isUp)
    {
        onParticleEvent?.Invoke(isUp);
    }
}
